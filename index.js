const websocket = require('ws');
const http = require('http');
require('dotenv').config();
const Nexus = require('./Nexus');

const httpServer = http.createServer((request, response) => {
    response.writeHead(404);
    response.end();
});

httpServer.listen(process.env.HTTP_SERVER_PORT, function() {
    process.stdout.write(`HTTP server is listening on port ${process.env.HTTP_SERVER_PORT}\r\n`);
});



const nexus = new Nexus();
nexus.init()
    .then(() => {

        process.stdout.write('Nexus ready.\r\n');
        const wsServer = new websocket.Server({
            server: httpServer
        });

        wsServer.on('connection', (ws) => {
            nexus.connect(ws);
        });
    });
