const Probe = require('./Probe');
const Admin = require('./Admin');
const { v4: uuidv4 } = require('uuid');
require('dotenv').config();
const MongoClient = require('mongodb').MongoClient;

class Nexus {

    constructor() {

        this.probes = [];
        this.admins = {};
        this.suppressRoundRobin = false;

    }

    async init() {

        await this.fetchProbes();
        await this.roundRobin();

    }

    /**
     * @returns {Promise<void>}
     */
    async fetchProbes() {

        const mongoClient = new MongoClient(process.env.MONGO_URL, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });

        const client = await mongoClient.connect();
        const db = client.db("qtest");
        const collection = db.collection("probes");

        const probes = await collection.find().toArray();
        await client.close();

        this.probes = probes.map(probeData => {
            const probe = new Probe(probeData.token);

            probe.on('disconnect', () => {
                this.probeDisconnectHandler(probeData.token);
            });

            probe.on('start', () => {
                this.probeStartHandler(probeData.token);
            });

            probe.on('report', (data) => {
                //data:
                // isSuccessful: true,
                //     isInterrupted: false,
                //     readyness: 81.4750891727074,
                //     pingDelayIsOk: true,
                //     pingLossRateIsOk: true,
                //     speedIsOk: true
            });
            return probe;
        });

    }

    async roundRobin() {

        if (!this.suppressRoundRobin) {
            let someStarted = this.probes.some(probe => {
                return probe.isStarted;
            });

            if (!someStarted) {
                let liveProbes = this.probes.filter(probe => {
                    return !!probe.ws;
                });

                liveProbes = liveProbes.sort((probeA, probeB) => {
                    return probeA.finishedAt - probeB.finishedAt;
                });

                if (liveProbes[0]) {
                    try {
                        await liveProbes[0].start();
                    }
                    catch (error) {
                        //DO nothing
                    }
                }
            }
        }

        setTimeout(async () => {
            await this.roundRobin();
        }, parseInt(process.env.ROUND_ROBIN_INTERVAL));

    }

    /**
     * @param ws
     * @returns {Promise<void>}
     */
    async connect(ws) {

        process.stdout.write('WS client connected.\r\n');

        let wsRole = null;

        ws.on('message', async (message) => {
            try {
                message = JSON.parse(message);

                if (wsRole) {
                    return;
                }

                switch (message.action) {
                    case "admin-auth":
                        if (await this.adminAuthHandler(message, ws)) {
                            wsRole = 'admin';
                        }
                        break;
                    case "probe-auth":
                        if (await this.probeAuthHandler(message, ws)) {
                            wsRole = 'probe';
                        }
                        break;
                }
            }
            catch (e) {
                //DO nothing
            }
        });

    }

    /**
     * @param message
     * @param ws
     * @returns {Promise<boolean>}
     */
    async adminAuthHandler(message, ws) {

        if (message.token === process.env.APP_SECRET) {
            process.stdout.write('WS client admin-auth success.\r\n');

            let uuid = uuidv4();
            let admin = new Admin(ws);
            this.admins[uuid] = admin;

            await Nexus.wsSend(ws, JSON.stringify({
                action: 'admin-auth',
                ok: true
            }));

            ws.on('close', () => {
                delete this.admins[uuid];
                process.stdout.write(`Admin [${uuid}] disconnected.\r\n`);
            });

            this.addAdminEventsListeners(admin);

            return true;
        }
        else {
            process.stdout.write('WS client admin-auth failed.\r\n');

            await Nexus.wsSend(ws, JSON.stringify({
                action: 'admin-auth',
                ok: false,
                error: 'Invalid credentials.'
            }));

            return false;
        }

    }

    addAdminEventsListeners(admin) {

        admin.on('create-probe', this.createProbeHandler.bind(this));

        admin.on('fetch-probes', this.adminFetchProbesHandler.bind(this, admin));

    }

    /**
     * @param message
     * @param ws
     * @returns {Promise<boolean>}
     */
    async probeAuthHandler(message, ws) {

        for (let probe of this.probes) {
            if (probe.token === message.token) {
                if (probe.ws) {
                    process.stdout.write(`Attempting duplicate authentication. Probe token: ${probe.token}.\r\n`);

                    await Nexus.wsSend(ws, JSON.stringify({
                        action: 'probe-auth',
                        ok: false,
                        error: 'Conflict: probe already connected.'
                    }));
                    return false;
                }
                else {
                    process.stdout.write(`Probe ${probe.token} connected.\r\n`);

                    probe.setWs(ws);

                    await Nexus.wsSend(ws, JSON.stringify({
                        action: 'probe-auth',
                        ok: true
                    }));

                    await this.adminsBroadcast(JSON.stringify({
                        action: 'probe-connected',
                        data: {
                            token: probe.token
                        }
                    }));

                    return true;
                }
            }
        }

        process.stdout.write('Probe-auth failed.\r\n');

        await Nexus.wsSend(ws, JSON.stringify({
            action: 'probe-auth',
            ok: false,
            error: 'Invalid credentials.'
        }));

        return false;

    }

    /**
     * @param data
     * @returns {Promise<void>}
     */
    async createProbeHandler(data) {

        const probe = new Probe(data.token);

        probe.on('disconnect', () => {
            this.probeDisconnectHandler(data.token);
        });

        this.probes.push(probe);

        process.stdout.write(`Probe created. Token: ${data.token} connected.\r\n`);

        await this.adminsBroadcast(JSON.stringify({
            action: 'probe-created',
            data
        }));

    }

    async adminFetchProbesHandler(admin) {

        await Admin.wsSend(admin.ws, {
            action: 'probes',
            data: this.probes.map(probe => {
                return {
                    connected: !!probe.ws,
                    token: probe.token,
                    isStarted: probe.isStarted,
                    lastReportAt: probe.finishedAt
                }
            })
        });

    }

    /**
     * @param token
     * @returns {Promise<void>}
     */
    async probeDisconnectHandler(token) {

        process.stdout.write(`Probe ${token} disconnected.\r\n`);

        await this.adminsBroadcast(JSON.stringify({
            action: 'probe-disconnect',
            token
        }));

    }

    /**
     * @param token
     * @returns {Promise<void>}
     */
    async probeStartHandler(token) {

        process.stdout.write(`Probe ${token} start test.\r\n`);

        await this.adminsBroadcast(JSON.stringify({
            action: 'probe-start-test',
            token
        }));

    }

    /**
     * @param ws
     * @param message
     * @returns {Promise<unknown>}
     */
    static wsSend(ws, message) {

        return new Promise((resolve, reject) => {
            try {
                ws.send(message, () => {
                    resolve();
                });
            }
            catch (error) {
                reject();
            }
        });

    }

    /**
     * @param message
     * @returns {Promise<void>}
     */
    async adminsBroadcast(message) {

        let promises = [];

        for (let uuid in this.admins) {
            promises.push(Nexus.wsSend(this.admins[uuid].ws, message));
        }

        await Promise.all(promises);

    }

}

module.exports = Nexus;
