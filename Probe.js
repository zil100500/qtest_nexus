const EventEmitter = require('events').EventEmitter;
require('dotenv').config();
const MongoClient = require('mongodb').MongoClient;

class Probe extends EventEmitter {

    constructor(token) {

        super();
        this.ws = null;
        this.token = token;
        this.isStarted = false;
        this.finishedAt = 0;

    }

    setWs(ws) {

        this.ws = ws;

        ws.on('message', (message) => {
            try {
                message = JSON.parse(message);
            }
            catch (e) {
                return;
            }

            switch (message.action) {
                case 'start':
                    this.startHandler(message);
                    break;
                case 'test':
                    switch (message.subAction) {
                        case 'report':
                            this.finishHandler(message);
                            break;
                    }
                    break;
                default:
                    console.log(message);
            }
        });

        ws.on('close', () => {

            this.ws = null;
            this.isStarted = false;
            this.emit('disconnect');

        });

    }

    async start() {

        await Probe.wsSend(this.ws, JSON.stringify({
            action: 'start'
        }));

        this.isStarted = true;

    }

    async startHandler() {
        this.emit('start');
    }

    async finishHandler(report) {

        report.data.timestamp = (new Date()).getTime();

        this.finishedAt = (new Date()).getTime();
        this.isStarted = false;
        this.emit('report', report.data);

        const mongoClient = new MongoClient(process.env.MONGO_URL, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });

        const client = await mongoClient.connect();
        const db = client.db("qtest");
        const collection = db.collection("reports");

        let insertResult = await collection.insertOne(report.data);

        await client.close();

    }

    interrupt() {
        //TODO
    }

    static wsSend(ws, message) {

        return new Promise((resolve, reject) => {
            try {
                ws.send(message, () => {
                    resolve();
                });
            }
            catch (error) {
                reject();
            }
        });

    }

}

module.exports = Probe;
