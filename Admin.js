const { v4: uuidv4 } = require('uuid');
const MongoClient = require('mongodb').MongoClient;
const EventEmitter = require('events').EventEmitter;

class Admin extends EventEmitter{

    constructor(ws) {

        super();
        this.ws = ws;

        this.ws.on('message', async (message) => {

            try {
                message = JSON.parse(message);
                switch (message.action) {
                    case 'create-probe':
                        await this.createProbeHandler();
                        break;
                    case 'fetch-reports':
                        await this.fetchReportsHandler(message.query);
                        break;
                    case 'fetch-probes':
                        this.emit('fetch-probes');
                        break;
                }
            }
            catch (e) {
                //DO nothing
            }

        });

    }

    async createProbeHandler() {

        const mongoClient = new MongoClient(process.env.MONGO_URL, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });

        const client = await mongoClient.connect();
        const db = client.db("qtest");
        const collection = db.collection("probes");

        let uuid = uuidv4();

        let insertResult = await collection.insertOne({
            token: uuid
        });

        await client.close();

        this.emit('create-probe', {
            id: insertResult.insertedId,
            token: uuid
        });

    }

    async fetchReportsHandler(query) {

        const mongoClient = new MongoClient(process.env.MONGO_URL, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });

        const client = await mongoClient.connect();
        const db = client.db("qtest");
        const collection = db.collection("reports");

        const reports = await collection.find(query).toArray();
        await client.close();

        await Admin.wsSend(this.ws, {
            action: 'reports',
            data: reports
        });

        console.log('done')

    }

    static wsSend(ws, message) {

        return new Promise((resolve, reject) => {
            try {
                ws.send(JSON.stringify(message), () => {
                    resolve();
                });
            }
            catch (error) {
                // reject();
                resolve();
            }
        });

    }

}

module.exports = Admin;
